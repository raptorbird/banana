//=========================//
//===== DATA BLOCKS =======//
//=========================//
Blockly.Blocks['digital_block'] = {
  init: function() {
    this.setColour(330);
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown([["High", "1"], ["Low", "0"]]), "DO");
    this.setInputsInline(true);
    this.setOutput(true);
    this.setTooltip('');
  }
}
Blockly.Python['digital_block'] = function(block) {
  code = block.getFieldValue('DO');
  return [code, Blockly.Python.ORDER_NONE]
}
Blockly.Blocks['digital_pin'] = {
  init: function() {
    this.setColour(330);
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown([["0", "0"], ["1", "1"], ["2", "2"], ["3", "3"], ["4", "4"], ["5", "5"], ["6", "6"], ["7", "7"]]), "PIN");
    this.setInputsInline(true);
    this.setOutput(true);
    this.setTooltip('');
  }
}
Blockly.Python['digital_pin'] = function(block) {
  code = block.getFieldValue('PIN');
  return [code, Blockly.Python.ORDER_NONE]
}
Blockly.Blocks['analog_pin'] = {
  init: function() {
    this.setColour(330);
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown([["0", "0"], ["1", "1"], ["2", "2"], ["3", "3"], ["4", "4"], ["5", "5"], ["6", "6"]]), "PIN");
    this.setInputsInline(true);
    this.setOutput(true);
    this.setTooltip('');
  }
}
Blockly.Python['analog_pin'] = function(block) {
  code = block.getFieldValue('PIN');
  return [code, Blockly.Python.ORDER_NONE]
}
Blockly.Blocks['servo_angle'] = {
  init: function() {
    this.setColour(330);
    this.appendDummyInput()
        .appendField(new Blockly.FieldAngle("90"), "angle");
    this.setInputsInline(true);
    this.setOutput(true);
    this.setTooltip('');
  }
}
Blockly.Python['servo_angle'] = function(block) {
  code = block.getFieldValue('angle');
  return [code, Blockly.Python.ORDER_NONE]
}
//=========================//
//===== DIGITAL WRITE =====//
//=========================//
Blockly.Blocks['digital_write'] = {
  init: function() {
    this.setHelpUrl('http://www.example.com/');
    this.setColour(330);
    this.appendDummyInput()
        .appendField("Digital Write");
    this.appendValueInput("DO")
        .setCheck("Boolean");
    this.appendDummyInput()
        .appendField(" to Pin ");
    this.appendValueInput("PIN");
    this.setInputsInline(true);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip('');
  }
};
Blockly.Python['digital_write'] = function(block) {
  var data = Blockly.Python.valueToCode(block, 'DO', Blockly.Python.ORDER_ATOMIC);
  var pin = Blockly.Python.valueToCode(block, 'PIN', Blockly.Python.ORDER_ATOMIC);
  var code = 'pyb.Pin("'+pin+'").value('+data+')\n';
  return code;
};
//=========================//
//===== DIGITAL READ ======//
//=========================//
Blockly.Blocks['digital_read'] = {
  init: function() {
    this.setHelpUrl('http://www.example.com/');
    this.setColour(330);
    this.appendDummyInput()
        .appendField("Digital Read Pin");
    this.appendValueInput("PIN");
    this.setInputsInline(true);
    this.setOutput(true);
    this.setTooltip('');
  }
};
Blockly.Python['digital_read'] = function(block) {
  var pin = Blockly.Python.valueToCode(block, 'PIN', Blockly.Python.ORDER_ATOMIC);
  var code = 'pyb.Pin("'+pin+'").value()';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Python.ORDER_NONE];
};
//=========================//
//===== ANALOG WRITE ======//
//=========================//
Blockly.Blocks['analog_write'] = {
  init: function() {
    this.setHelpUrl('http://www.example.com/');
    this.setColour(0);
    this.appendDummyInput()
        .appendField("Analog Write");
    this.appendValueInput("AO")
        .setCheck("Number");
    this.appendDummyInput()
        .appendField("to Pin ");
    this.appendValueInput("PIN");
    this.setInputsInline(true);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip('');
  }
};
Blockly.Python['analog_write'] = function(block) {
  var data = Blockly.Python.valueToCode(block, 'AO', Blockly.Python.ORDER_ATOMIC);
  var pin = Blockly.Python.valueToCode(block, 'PIN', Blockly.Python.ORDER_ATOMIC);
  var code = 'pyb.DAC('+pin+').write('+data+')\n';
  return code;
};
//=========================//
//===== ANALOG READ =======//
//=========================//
Blockly.Blocks['analog_read'] = {
  init: function() {
    this.setHelpUrl('http://www.example.com/');
    this.setColour(0);
    this.appendDummyInput()
        .appendField("Analog Read Pin");
    this.appendValueInput("PIN");
    this.setInputsInline(true);
    this.setOutput(true);
    this.setTooltip('');
  }
};
Blockly.Python['analog_read'] = function(block) {
  var pin = Blockly.Python.valueToCode(block, 'PIN', Blockly.Python.ORDER_ATOMIC);
  var code = 'pyb.ADC('+pin+').read()';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Python.ORDER_NONE];
};
//=========================//
//===== SERVO WRITE =======//
//=========================//
Blockly.Blocks['servo_write'] = {
  init: function() {
    this.setHelpUrl('http://www.example.com/');
    this.setColour(330);undefined
    this.appendDummyInput()
        .appendField("Servo ");
    this.appendValueInput("PIN");
    this.appendDummyInput()
        .appendField(" Position ");
    this.appendValueInput("angle")
        .setCheck("Number");
    this.setInputsInline(true);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip('');
  }
};
Blockly.Python['servo_write'] = function(block) {
  var pin = Blockly.Python.valueToCode(block, 'PIN', Blockly.Python.ORDER_ATOMIC);
  var angle = Blockly.Python.valueToCode(block, 'angle', Blockly.Python.ORDER_ATOMIC);
  var code = 'pyb.Servo('+pin+').angle('+angle+',time=0)\n';
  return code;
};
//=========================//
//======= DELAY ===========//
//=========================//
Blockly.Blocks['wait'] = {
  init: function() {
    this.setHelpUrl('http://www.example.com/');
    this.appendDummyInput()
        .appendField("Wait")
    this.appendValueInput("TIME");
    this.appendDummyInput()
        .appendField(" ")
        .appendField(new Blockly.FieldDropdown([["second(s)", "1"], ["milisecond(s)", "0"]]), "type");
    this.setInputsInline(true);
    this.setPreviousStatement(true, "null");
    this.setNextStatement(true, "null");
    this.setTooltip('');
  }
};
Blockly.Python['wait'] = function(block) {
  var time = Blockly.Python.valueToCode(block, 'TIME', Blockly.Python.ORDER_ATOMIC);
  var type = block.getFieldValue('type');
  if (type === "1"){
    var code = 'pyb.delay('+time+')\n';
  } else {
    var code = 'pyb.udelay('+time+')\n';
  }
  return code;
};
//=========================//
//========= MAP ===========//
//=========================//
Blockly.Blocks['map'] = {
  init: function() {
    this.setHelpUrl('http://www.example.com/');
    this.setColour(230);
    this.appendDummyInput()
        .appendField("Map Value:");
    this.appendValueInput("input")
        .setCheck("Number");
    this.appendDummyInput()
        .appendField("From Range")
        .appendField(new Blockly.FieldTextInput("0"), "fromLo")
        .appendField(":")
        .appendField(new Blockly.FieldTextInput("0"), "fromUp");
    this.appendDummyInput()
        .appendField("To Range")
        .appendField(new Blockly.FieldTextInput("0"), "toLo")
        .appendField(":")
        .appendField(new Blockly.FieldTextInput("0"), "toUp");
    this.setOutput(true, "Number");
    this.setTooltip('');
  }
};
Blockly.Python['map'] = function(block) {
  var i = Blockly.Python.valueToCode(block, 'input', Blockly.Python.ORDER_ATOMIC);
  var fl = block.getFieldValue('fromLo');
  var fu = block.getFieldValue('fromUp');
  var tl = block.getFieldValue('toLo');
  var tu = block.getFieldValue('toUp');
  var code = 'board.translate('+i+', '+fl+', '+fu+', '+tl+', '+tu+')';
  return [code, Blockly.Python.ORDER_NONE];
};
//=========================//
//======== TONE ===========//
//=========================//
Blockly.Blocks['tone'] = {
  init: function() {
    this.setHelpUrl('http://www.example.com/');
    this.setColour(230)
    this.appendDummyInput()
        .appendField("Tone ")
    this.appendValueInput("TONE");
    this.appendDummyInput()
        .appendField(' at Pin ')
        .appendField(new Blockly.FieldDropdown([["0", "0"], ["1", "1"]]), "PIN");
    this.setInputsInline(true);
    this.setPreviousStatement(true, "null");
    this.setNextStatement(true, "null");
    this.setTooltip('');
  }
};
Blockly.Python['tone'] = function(block) {
  var tone = Blockly.Python.valueToCode(block, 'TONE', Blockly.Python.ORDER_ATOMIC);
  var pin = block.getFieldValue('PIN');
  var code = 'board.tone('+pin+', '+tone+')\n';
  return code;
};

//=========================//
//======== ONLOAD =========//
//=========================//
onload = function() {
    Blockly.inject(document.body, {
        path: '',
        toolbox: document.getElementById('toolbox')
    });
    window.parent.Blockly = Blockly;
    window.showCode = function() {
        console.log(Blockly.Python.workspaceToCode());
    };
    window.saveToFile = function() {
        var xml = Blockly.Xml.workspaceToDom(Blockly.mainWorkspace);
        var xml_text = Blockly.Xml.domToText(xml);
        editor.consoleLog('WRITING: ' + xml_text);
        return xml_text;
    };
    window.openFromFile = function(xml_text) {
        editor.consoleLog('READING: ' + xml_text);
        var xml = Blockly.Xml.textToDom(xml_text);
        Blockly.Workspace.clear();
        Blockly.Xml.domToWorkspace(Blockly.mainWorkspace, xml);
    };
    Blockly.addChangeListener(window.showCode);
};
